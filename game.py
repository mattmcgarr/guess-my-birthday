from random import randint

user_name = input("Hi! What is your name? ")



for guess_number in range(1,6):
    number_months = randint(1,12)
    number_year = randint(1924,2004)


    print("Guess", guess_number, ":", user_name, "were you born on ", number_months, " / ", number_year, "?")


    response = input("Yes or no? ")


    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again!")
